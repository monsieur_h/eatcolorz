--- Gamestate "gameover" 
gameover	=   Gamestate.new()


function gameover:enter()
	print("Game is over")
	bigfont	=	love.graphics.newFont( 52 )
	smallfont=	love.graphics.newFont( 14 )
end


function gameover:update( dt )
	if love.keyboard.isDown("return") then
		Gamestate.switch( play )
	elseif love.keyboard.isDown("escape") then
        love.event.quit()
	end
end

function gameover:draw()
	love.graphics.setColor(0, 0, 0, 0)
	love.graphics.rectangle( "fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight() )


	love.graphics.setColor(255, 255, 255, 255)
	love.graphics.setFont( bigfont )
	love.graphics.printf("Game Over", 0, love.graphics.getHeight()/3, love.graphics.getWidth(), "center")
	
	love.graphics.setFont( smallfont )
    if player.deathreason then
        love.graphics.printf(player.deathreason, 0, love.graphics.getHeight()/2, love.graphics.getWidth(), "center")
    end
	love.graphics.printf("Press Enter to retry, escape to quit", 0, love.graphics.getHeight()/1.7, love.graphics.getWidth(), "center")
    love.graphics.printf("You scored "..player.score, 0, love.graphics.getHeight()/1.8, love.graphics.getWidth(), "center")
end
