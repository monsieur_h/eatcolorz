--- Gamestate "play" occurs when we are actually playing the game as opposed to "menu" or "paused"
play        =   Gamestate.new()


function play:enter()
    EATABLES    =   {}
    COLLIDABLES =   {}
	player:setPosition( 100, 200 )
	player.dead	=	false
    player.score=   0
    player:resetColor()
    player.pos  =   vector(0, 0)
    local tmp   =   love.filesystem.load( "res/level.lua" )()
    tmp:init()
    tmp:setPlayer( player )
    COLLIDABLES =   tmp.platforms
    EATABLES    =   tmp.powerups 
    -- Giving the list of eatable things to our player:
    player:setEatables( EATABLES )
    player:setCollidables( COLLIDABLES )
end

function play:update( dt )
    player:update ( dt )
    camera:track( player )
	if player.dead then
		Gamestate.switch( gameover )
	end

    for i, item in pairs( EATABLES ) do
        item:update( dt )
        if item.dead then
            table.remove( EATABLES, i) -- On dégage l'item avec son indice s'il est mort
        end
    end
    for i, item in pairs( COLLIDABLES ) do
        item:update( dt )
        if item.dead then
            table.remove( COLLIDABLES, i) -- On dégage l'item avec son indice s'il est mort
        end
    end

end

function play:draw()
    --On dessine le fond, genre la même couleur que le joueur
    love.graphics.setColor( player.color.r/2, player.color.g/2, player.color.b/2, 180 )
    love.graphics.rectangle( "fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight() )
    

    camera:attach()
    love.graphics.setLine( 10, "smooth" )
    love.graphics.setColor( 0, 0, 0, 255 )
    local px, py    =   camera:cameraCoords( player.pos:unpack() )
    love.graphics.line( player.pos.x - love.graphics.getWidth(), love.graphics.getHeight(), player.pos.x + love.graphics.getWidth(), love.graphics.getHeight() )
    --Ondessine les plateformes
    for i, item in pairs( COLLIDABLES ) do
        item:draw()
    end

    --On dessine le joueur
    player:draw()

    --On dessine les objets
    for i, item in pairs( EATABLES ) do
        item:draw()
    end



    camera:detach()
    if DEBUG then
        local text = "FPS:"..love.timer.getFPS()
        text    = text.."\nPosition du joueur: ".."\n"..player.pos.x.." "..player.pos.y
        text    =   text.."\nPlayer colors:"
        text    =   text.."\nR:"..player.color.r
        text    =   text.."\nG:"..player.color.g
        text    =   text.."\nB:"..player.color.b
        text    =   text.."\nSum:"..player.color.b+player.color.r+player.color.g
        love.graphics.print( text, 10, 10 )
    end
end
