-- Introduction showing the love thing, and then the instruction on the game concept

intro   =   Gamestate.new()

function intro:init()
    img_love    =   love.graphics.newImage( "res/lovepowered.png" )
    img_rules   =   love.graphics.newImage( "res/intro.png" )

    current_image   =   img_love
    fadetime        =   2 --second
    introtime       =   fadetime * -1

end

function intro:update( dt )
    introtime   =   introtime + dt
    if introtime > 0 then
            current_image   =   img_rules
    end
    local d     =   math.abs( introtime )

    imgalpha    =   clamp( d, 0, fadetime )
    imgalpha    =   imgalpha / fadetime
    imgalpha    =   imgalpha * 255

end

function intro:draw()

    love.graphics.setColor( 255, 255, 255, imgalpha )
    love.graphics.draw( current_image, love.graphics.getWidth()/2 - current_image:getWidth() / 2, love.graphics.getHeight()/2 - current_image:getHeight() / 2 )
end

function intro:keypressed( key )
    if key == "return" then
        Gamestate.switch( play )
	elseif key == "n" then
		--Gamestate.switch( edit )
    end
end
