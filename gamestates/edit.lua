edit    =   Gamestate.new()

function edit:enter()
	mouse	=	{pressed=nil, released=vector.new(), previous=vector.new(), current=vector.new()}
	love.mouse.setGrab( true )
end

function edit:init()
    lil_font        =   love.graphics.newFont( 16 )
    -- Liste: Platform, deathtrap, eatable (color, value)
    level           =   {}
    self.buttons    =   {}
	self.items		=	{platforms={}, powerups={}, deathtraps={}}
    edit.mode       =   "default"
    addPlatform =   MenuItem( "Platform" )
    addPlatform:setCenter( 80, 90 )
    table.insert( self.buttons, addPlatform )
    function addPlatform:onClick()
        --TODO: create new platform here
        edit.mode   =   "platform"
    end

    addDeathTrap    =   MenuItem( "DeathTrap" )
    addDeathTrap:setCenter( 80, 30 )
    table.insert( self.buttons, addDeathTrap )
    function addDeathTrap:onClick()
        --TODO: create new deathtrap here
        edit.mode   =   "deathtrap"
    end

    addEatableItem  =   MenuItem( "EatableItem" )
    addEatableItem:setCenter( 80, 60 )
    table.insert( self.buttons, addEatableItem )
    function addEatableItem:onClick()
        --TODO: create new deathtrap here
        edit.mode   =   "eatableitem"
    end
end

function edit:update( dt )
	mouse.current.x, mouse.current.y = love.mouse.getPosition()

    for i, button in pairs( self.buttons ) do
        button:update( dt )
        if button:contains( mouse.current.x, mouse.current.y ) then
            button:onHover()
        end
    end
	-- Updating les eatables items
    --for i, item in pairs( self.items.powerups ) do
		-- item:update( dt )
	-- end

    if love.mouse.isDown( "l" ) then
        if mouse.previous then
        else
            mouse.previous = {}
        end

        mouse.previous.x, mouse.previous.y = mouse.current.x, mouse.current.y
    else
        mouse.previous = nil
        --seedcam:update(dt, m_x, m_y)
    end
    
    --Update camera position
    moveCamera( dt, camera )
end

function edit:draw()
    --Le fond
	love.graphics.setColor( 200, 200, 200, 200 )
	love.graphics.rectangle( "fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight() )
    
    --Le sol
    camera:attach()
    love.graphics.setLine( 10, "smooth" )
    love.graphics.setColor( 0, 0, 0, 255 )
    local cx, cy = camera:pos()
    love.graphics.line( cx - love.graphics.getWidth(), love.graphics.getHeight(), cx + love.graphics.getWidth(), love.graphics.getHeight() )
    

    --Dessiner les diff�rents objets du level
    for i, item in pairs( self.items.powerups) do
		item:draw()
	end
    for i, item in pairs( self.items.deathtraps) do
		item:draw()
	end
    for i, item in pairs( self.items.platforms ) do
		item:draw()
	end
	-- HUDE ELEMENTS, non li�s � la cam�ra
	camera:detach()
    --Dessiner les boutons
    for i, button in pairs( self.buttons ) do
        button:draw()
    end
    love.graphics.setFont( lil_font )
    love.graphics.setColor( 255, 255, 255, 255 )
    love.graphics.print( "Adding : "..edit.mode, 100, love.graphics.getHeight() - 30  )

    --Le rectangle de creation du module courant
	if mouse.pressed and love.mouse.isDown( "l" ) and ( edit.mode == "deathtrap" or edit.mode == "platform" ) then
		love.graphics.setColor( 0, 0, 255, 125 )
		love.graphics.rectangle( "fill", mouse.pressed.x, mouse.pressed.y, math.floor( mouse.current.x - mouse.pressed.x ) , mouse.current.y - mouse.pressed.y )
	end
end


function edit:mousepressed( m_x, m_y, button )
    mouse.pressed   = vector.new()
    mouse.pressed.x = m_x
    mouse.pressed.y = m_y
	if button == "r" then
	--Ajouter
	for i, but in pairs( self.buttons ) do
		if but:contains( m_x, m_y ) then
			but:onClick()
		end
	end
	elseif button == "l" then
	end
end

function edit:mousereleased( m_x, m_y, button )
	if button == "l" then
		createElement( mouse, edit.mode, self.items, camera )
		if mouse.pressed then mouse.pressed = nil end
	end
end

function edit:keypressed( key, code )
    if key=="return" then
        print( "Saving level to file" )
        saveLevelToFile( self.items )
    end
end

function createElement( p_mouse, p_mode, p_items, p_camera )
	if p_mode == "platform" then
		if not p_camera then
			w	=	p_mouse.current.x - p_mouse.pressed.x
			h	=	p_mouse.current.y - p_mouse.pressed.y
			newItem	=	Platform( w, h, p_mouse.pressed.x + w / 2, p_mouse.pressed.y + h / 2 )
		else
			local px, py	=	p_camera:worldCoords( p_mouse.pressed.x, p_mouse.pressed.y )
			local cx, cy	=	p_camera:worldCoords( p_mouse.current.x, p_mouse.current.y )
			w	=	cx - px
			h	=	cy - py
			newItem	=	Platform( w, h, px + w / 2, py + h / 2 )
		end
		if not p_items.platforms then p_items.platforms={} end
		table.insert( p_items.platforms, newItem )

	elseif p_mode == "deathtrap" then
		if not p_camera then
			w	=	p_mouse.current.x - p_mouse.pressed.x
			h	=	p_mouse.current.y - p_mouse.pressed.y
			newItem	=	DeathTrap( w, h, p_mouse.pressed.x + w / 2, p_mouse.pressed.y + h / 2 )
		else
			local px, py	=	p_camera:worldCoords( p_mouse.pressed.x, p_mouse.pressed.y )
			local cx, cy	=	p_camera:worldCoords( p_mouse.current.x, p_mouse.current.y )
			w	=	cx - px
			h	=	cy - py
			newItem	=	DeathTrap( w, h, px + w / 2, py + h / 2 )
		end
		if not p_items.deathtraps then p_items.deathtraps={} end
		table.insert( p_items.deathtraps, newItem )
		
	elseif p_mode == "eatableitem" then
		local px, py	=	p_camera:worldCoords( p_mouse.current.x, p_mouse.current.y )
		local newItem	=	EatableItem( px, py, 25, "random" )
		table.insert( p_items.powerups, newItem )
--function EatableItem:__init__( p_x, p_y, p_value, p_colorstring )
	else
		return false
	end
end
