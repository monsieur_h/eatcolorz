-- Slither for OOP
require "lib.slither"
--HUMP lib (vrld)
Gamestate = require "lib.hump.gamestate"
ringbuffer= require "lib.hump.ringbuffer"
camera    = require "lib.hump.camera"
vector    = require "lib.hump.vector"

---------gamestates
require "gamestates.play"
require "gamestates.gameover"
require "gamestates.edit"
require "gamestates.intro"



----------classes
require "class.Eater"
require "class.EatableItem"
require "class.Rectangle"
require "class.Platform"
require "class.DeathTrap"
require "class.Particle"
require "class.MenuItem"


APP_NAME        =   "Eat Colorz !"
PROJECT_VERSION =   " 0.0.1"
DEBUG           =   false
SCROLL_SPEED    =   500

function love.load()
    player = Eater()
    camera = camera(0, 0, 0.5, 0) -- Camera at origin point, with no zoom*
    camera:lookAt( 0, love.graphics.getHeight()*camera.zoom - 200)
    function camera:track( p_player )
        self:lookAt( p_player.pos.x, self.y )

    end
    -- Init gamestates and display
    Gamestate.registerEvents()
    Gamestate.switch( intro )
end

function love.keyreleased( key, unicode )

    if key == "escape" then
        love.event.quit()
    -- elseif key == "f1" or key == "d"  then
        -- print( "toggling debug" )
        -- DEBUG   =   not DEBUG
        -- print( DEBUG )        
    end
end

function love.quit()
    print( "Thanks for playing ".. APP_NAME )

end

---------------------------------
-----------UTIL FUNCTIONS
---------------------------------

function clamp(input, min_val, max_val)
    if input < min_val then
        input = min_val
    elseif input > max_val then
        input = max_val
    end
    return input
end

function tprint (tbl, indent)
  if not indent then indent = 0 end
  for k, v in pairs(tbl) do
    formatting = string.rep("  ", indent) .. k .. ": "
    if type(v) == "table" then
      print(formatting)
      tprint(v, indent+1)
    else
      print(tostring(formatting) .. tostring(v))
    end
  end
end

function controlRed( p_red, p_status )
    if p_red >=255 or p_red <= 0 then
        if p_status then
            print("Red is: "..p_red.. " at "..p_status)
        end
    end
end

function saveLevelToFile( p_items )
    assert( p_items )
    print( p_items )
    print( #p_items )
    textString	=	""
    for i, v in pairs( p_items.platforms ) do
        textString = textString .. v:toConstructorString().."\n"
    end
    for i, v in pairs( p_items.deathtraps ) do
        textString = textString .. v:toConstructorString().."\n"
    end
    for i, v in pairs( p_items.powerups ) do
        textString = textString .. v:toConstructorString().."\n"
    end
    love.filesystem.write( "level.lua", textString )
    print( "Saved to : ".. love.filesystem.getSaveDirectory().."level.lua" )
    print( "Content  : "..textString )
end

function moveCamera( dt, p_camera )
    assert( p_camera )
    local key   =   love.keyboard
    local x, y  =   love.mouse.getPosition()
    --Left test
    if key.isDown( "left" ) or x <= 0 then
        p_camera:move( -SCROLL_SPEED * dt, 0 )
    end

    if key.isDown( "right" ) or x >= love.graphics.getWidth()-10 then
        p_camera:move( SCROLL_SPEED * dt, 0 )
    end

    if key.isDown( "up" ) or y <=  0 then
        p_camera:move( 0, -SCROLL_SPEED * dt )
    end

    if key.isDown( "down" ) or y >= love.graphics.getHeight()-10 then
        p_camera:move( 0, SCROLL_SPEED * dt )
    end
end
