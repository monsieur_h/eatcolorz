--Level du jeu EatColorz
level           =   {}
level.platforms =   {}
level.powerups  =   {}
level.deathtraps=   {}
-- Platforms ( longueur, largeur, centre X, 600-centreY)
love.filesystem.load("res/leveldata.lua")()

-- Dernier item a manger pour finir le level
finishItem =  EatableItem( 6395, 270.5, 25, "red" )
finishItem.color = player.color
function finishItem:getsEaten()
    player.score    =   player.score * 1.2
    player:die( "You finished the prototype level" )
end
table.insert( level.powerups, finishItem )

function level:setPlayer( p_player )
    for i, pl in pairs( self.platforms ) do
        pl:setPlayer( p_player )
    end
end
--Lier les powerups aux plateformes sur la m�me plage de X pour qu'il puissent tomber dessus.
--Pas besoin de lier les autres, vu que �a tombe toujours droit.
function level:init() 
    for i, eatable in pairs( self.powerups ) do
        for j, pl in pairs( self.platforms ) do
            if pl.rect.x <= eatable.pos.x and pl.rect.x+pl.rect.width >= eatable.pos.x then
                eatable:addCollidable( pl )
            end
        end
    end

end

return level
