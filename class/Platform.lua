-- Util class Platform to keep track of coordinates and make quick tests

require "class.Rectangle"
class "Platform"{
}

function Platform:__init__(p_width, p_height, c_x, c_y)
    assert(p_width, "Platform needs width")
    assert(p_height, "Platform needs height")
    assert(c_x, "Need center X")
    assert(c_y, "Need center Y")
    self.rect   =   Rectangle( 0, 0, p_width, p_height )
    self.time   =   0
    self.pos    =   vector( 0, 0 )
    self.particles  =   nil
    self:setCenter( c_x, c_y )
    self.particles  =   nil
end

function Platform:update( dt )
    self.time   =   self.time + dt
    if self.particles then self:updateParticles( dt ) end
end

function Platform:draw()
    --Dessinner le rectangle TODO: couleur a revoir
    if self.color then
        love.graphics.setColor( self.color.r, self.color.g, self.color.b, 255 )
    elseif self.player then
        love.graphics.setColor( self.player.color.r, self.player.color.g, self.player.color.b, 255 )
    else
        love.graphics.setColor( 0, 0, 0, 255 )
    end
    love.graphics.setLine( 10, "smooth" )
    love.graphics.rectangle( "fill", self.rect.x, self.rect.y, self.rect.width, self.rect.height )
    --Dessinner la ligne de contour
    love.graphics.setColor( 0, 0, 0, 255 )
    self.rect:draw()
    if self.particles then self:drawParticles() end
end

function Platform:setCenter( p_x, p_y )
    self.rect:setCenter( p_x, p_y )
    self.pos    =   vector( p_x, p_y )
end

function Platform:collides( p_x, p_y )
    if      p_x >= self.rect.x
        and p_x <= self.rect.x + self.rect.width
        and p_y >= self.rect.y
        and p_y <= self.rect.y + 50
    then
        return true
    else
        return false
    end
end

function Platform:setPlayer( p_player )
    assert( p_player )
    self.player =   p_player
end

function Platform:playerBind( p_player )
    assert( p_player )
    if not self.color then
        self.color  =   {}
        self.color.r, self.color.g, self.color.b    =   p_player.color.r, p_player.color.g, p_player.color.b
        self:setPlayer( p_player )
        self:initParticles()
    end

end

function Platform:initParticles()
    self.particles  =   {}
    local xoffset   =   20
    local yoffset   =   20
    local number    =   self.rect.width / xoffset
    local direction =   vector.new( 0, -1 ) --particules vers le haut
    for i=1, number do
        a   =   Particle( self.rect.x + xoffset * i, self.rect.y )
        a:setDirection( 0, -1 )
        a:setLifeTime( 0.5 )
        a:setColor( self.color )
        table.insert( self.particles, a )
    end

    for i=1, number do
        a   =   Particle( self.rect.x + xoffset * i, self.rect.y+self.rect.height )
        a:setDirection( 0, 1 )
        a:setLifeTime( 0.5 )
        a:setColor( self.color )
        table.insert( self.particles, a )
    end

    local number    =   self.rect.height / yoffset
    for i=1, number do
        a   =   Particle( self.rect.x, self.rect.y+yoffset*i )
        a:setDirection( -1, 0 )
        a:setLifeTime( 0.5 )
        a:setColor( self.color )
        table.insert( self.particles, a )
    end
    for i=1, number do
        a   =   Particle( self.rect.x+self.rect.width, self.rect.y+yoffset*i )
        a:setDirection( 1, 0 )
        a:setLifeTime( 0.5 )
        a:setColor( self.color )
        table.insert( self.particles, a )
    end
end

function Platform:drawParticles()
    for i, p in pairs( self.particles ) do
        p:draw()
    end
end

function Platform:updateParticles( dt )
    for i, p in pairs( self.particles ) do
        p:update( dt )
        if p.dead then
            table.remove( self.particles, i )
        end
    end
end

function Platform:toConstructorString()
	local cx, cy	=	self.rect:getCenter()
	return "table.insert( level.platforms, Platform( "..self.rect.width..", "..self.rect.height..", "..cx..", "..cy.." ) )"
end
