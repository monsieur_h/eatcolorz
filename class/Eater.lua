------------------------------------------------------------------------
-- @class Eater
-- Class representing the player
------------------------------------------------------------------------


-- La quantité de rouge influe sur
-- La quantité de vert influe sur la vitesse
-- La quantité de bleu influe sur le saut / la gravité

class "Eater"{
}

function Eater:__init__()
    local i_body      = love.graphics.newImage( "res/player_body.png" )
    local i_eatface   = love.graphics.newImage( "res/player_eat.png" )
    local i_idleface  = love.graphics.newImage( "res/player_idle.png" )
    local i_fearface  = love.graphics.newImage( "res/player_fear.png" )
    self.dead       =   false
    self.pos        =   vector( 200, 200 )
    self.color      =   {r=50, g=170, b=170}
    self.life       =   100
    self.time       =   0
    self.offset     =   {x=0, y=0}
    self.jumping    =   false
    self.last_jump  =   0
    self.jump_len   =   0.4 --second
    self.last_eat   =   0
    self.eat_len    =   0.25
    self.entropy    =   25
    self.speed      =   {jump=1000, walk=500}
    self.image      =   {body=i_body, eatface=i_eatface, idleface=i_idleface, fearface=i_fearface}
    self.offset.x   =   self.image.body:getWidth()/2
    self.offset.y   =   self.image.body:getHeight()/2
    self.eatables   =   nil -- tableau d'items mangeables
    self.eating_range   =   120
    self.eaten_elements =   {}
    self.score      =   0

end

function Eater:resetColor()
    self.color      =   {r=50, g=170, b=170}
end

function Eater:update( dt )
    self.time   =   self.time + dt
    local sinceLastLunch    =   self.time - self.last_eat
    if love.keyboard.isDown( "left" ) then
        self:goLeft( dt )
    elseif love.keyboard.isDown( "right" ) then --right
        self:goRight( dt )
    elseif love.keyboard.isDown( "down" ) and sinceLastLunch >= self.eat_len*2 then  --eat?
        self:eat()
    end

    if love.keyboard.isDown( "up" ) then -- La touche up rend le gameplay plus fluide je trouve
        self:jump( dt )
    end

    if self.jumping then--Si on saute
        local delta     =   self.time - self.last_jump
        if delta > self.jump_len then
            self.jumping = false
        end
        local tmp   =   delta / self.jump_len
        tmp         =   1 - tmp
        tmp         =   tmp
        local factor=   self.color.b / 255
        self.pos.y  =   self.pos.y - self.speed.jump * dt * tmp * (factor + 0.5)
    end
    if self.eating then --Si on mange...
        local delta     =   self.time - self.last_eat
        if delta >= self.eat_len then --On a fini de manger, on remet à jour notre couleur
            self.eating = false
            for idx, elt in pairs(self.eaten_elements) do
                if elt.colorstring == "green" then
                    self.color.g    =   self.color.g + elt.value
                    self.color.r    =   self.color.r - elt.value
                    self.color.r    =   clamp(self.color.r, 0, 255)

                elseif elt.colorstring == "blue" then
                    self.color.b    =   self.color.b + elt.value
                    self.color.r    =   self.color.r - elt.value
                    self.color.r    =   clamp(self.color.r, 0, 255)
                else
                    self.color.r    =   self.color.r + elt.value
                end
            end
            self.eaten_elements =   {}
        end
    else
        self.color.r    =   self.color.r + self.entropy * dt
        self.color.g    =   self.color.g - self.entropy * dt / 2
        self.color.b    =   self.color.b - self.entropy * dt / 2
    end
    self.color.r    =   clamp(self.color.r, 0, 255)
    self.color.g    =   clamp(self.color.g, 0, 255)
    self.color.b    =   clamp(self.color.b, 0, 255)
    if self.color.r == 255 and self.color.g == 0 and self.color.b == 0 then
        self:die( "You died of entropy" )
    end

    if not self.jumping and not self:isOnGround() then
        self:fall( dt )
    end
end

function Eater:jump( dt )
    if not self.jumping  and self:isOnGround() and not self.eating then
        self.jumping    =   true
        self.last_jump  =   self.time
    end

end

function Eater:goLeft( dt )
    if self.eating then return end
    local factor    =   self.color.g / 255
    self.pos.x  =   self.pos.x - self.speed.walk * dt * (factor + 0.5)
end

function Eater:goRight( dt )
    if self.eating then return end
    local factor    =   self.color.g / 255
    self.pos.x  =   self.pos.x + self.speed.walk * dt * (factor + 0.5)
end

function Eater:draw()
    love.graphics.setColor( self.color.r, self.color.g, self.color.b, 255 )
    love.graphics.draw( self.image.body, self.pos.x, self.pos.y , 0, 1, 1, self.offset.x, self.offset.y )

    love.graphics.setColor(255, 255, 255, 255)

    local scalefactor   =   self:getEntropyLevel()
    scalefactor         =   1.5 - scalefactor
    if self.eating then
        love.graphics.draw( self.image.eatface, self.pos.x, self.pos.y , 0, scalefactor, scalefactor, self.offset.x, self.offset.y )
    else
        love.graphics.draw( self.image.idleface, self.pos.x, self.pos.y , 0, scalefactor, scalefactor, self.offset.x, self.offset.y )
    end
    love.graphics.setColor( 255, 255, 255, 255 * self:getEntropyLevel() )
    love.graphics.draw( self.image.fearface, self.pos.x, self.pos.y , 0, scalefactor, scalefactor, self.offset.x, self.offset.y )
    
    
    if DEBUG then
        lFoot  = vector( self.pos.x - self.offset.x, self.pos.y + self.offset.y )
        rFoot  = vector( self.pos.x + self.offset.x, self.pos.y + self.offset.y )
        love.graphics.setColor(255, 255, 255, 155)
        love.graphics.circle( "fill", self.pos.x, self.pos.y, 20 )
        love.graphics.circle( "fill", rFoot.x, rFoot.y, 20 )
        love.graphics.circle( "fill", lFoot.x, lFoot.y, 20 )
    end
end

function Eater:setPosition( p_x, p_y )
    self.pos    =   vector(p_x, p_y)
end

function Eater:isOnGround()
    local mylFeet = vector( self.pos.x-self.offset.x, self.pos.y + self.offset.y )
    local myrFeet = vector( self.pos.x+self.offset.x, self.pos.y + self.offset.y )
    local mymFeet = vector( self.pos.x, self.pos.y + self.offset.y )
    -- Vérifier si 1) on touche le bas de l'écran 2) on touche une plateforme
    if mylFeet.y  >=  love.graphics.getHeight() or myrFeet.y  >=  love.graphics.getHeight() or mymFeet.y  >=  love.graphics.getHeight() then
        return true
    end

    if self.collidables then
        for i, platform in pairs(self.collidables) do
            if platform:collides(mylFeet.x, mylFeet.y) or platform:collides(myrFeet.x, myrFeet.y) or platform:collides(mymFeet.x, mymFeet.y) then
                if platform.dangerous then
                    self:die( "You got reddened to death by a deathtrap" )
                else
                    self.pos.y  =   platform.rect.y - self.offset.y
                    platform:playerBind( self )
                    self.score  =   self.score + 10
                end
                return true
            end
        end
        return false
    end
end

function Eater:fall( dt )
    self.pos.y      =   self.pos.y + self.speed.jump * dt
end

function Eater:eat()
    if not self.eating then
        self.eating     =   true
        self.last_eat   =   self.time
        if self.eatables then
            local colorsum   =   {r=0, g=0, b=0}
            local mouthpos  =   self.pos:clone()
            mouthpos.x      =   mouthpos.x + self.offset.x/2 -- On part du principe que la bouche est sur le côté
            mouthpos.y      =   mouthpos.y + self.offset.y/1.5 --et en bas a droite!
            for idx, obj in pairs(self.eatables) do
                if mouthpos:dist(obj.pos) < self.eating_range then
                    obj:getsEaten( mouthpos, self.eat_len / 2 ) -- La disparition se fait 2 fois plus vite que l'animation de miam
                    table.insert(self.eaten_elements, obj)
                    self.score  =   self.score + obj.value
                end
            end
        end
    end
end

function Eater:setEatables( p_eatables )
    assert( p_eatables )
    self.eatables   =   p_eatables
end

function Eater:setCollidables( p_collidables )
    assert( p_collidables )
    self.collidables    =   p_collidables
end

function Eater:die( p_reason )
    self.dead       =   true
    self.deathreason= p_reason or nil
end

function Eater:getEntropyLevel()
    return self.color.r / 255
end
