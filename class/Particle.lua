

class "Particle"{
}

function Particle:__init__( p_x, p_y )
    self.pos    =   vector( p_x, p_y  )
    self.dead   =   false
    self.color  =   nil
    self.direction  =   nil
    self.time       =   0
    self.lifetime   =   1.0
    self.alpha      =   255
    self.speed      =   100
end

function Particle:update( dt )
    self.time   =   self.time + dt
    if self.time > self.lifetime then self.dead = true end
    self.alpha  =   255 - (self.time/self.lifetime*255)
    self.pos    =   self.pos + (self.direction * dt * self.speed)
end

function Particle:draw()
    love.graphics.setColor( self.color.r, self.color.g, self.color.b, self.alpha )
    love.graphics.circle( "fill", self.pos.x, self.pos.y, 10 )
end

function Particle:setColor( p_color )
    self.color  =   p_color
end

function Particle:setDirection( p_x, p_y )
    assert( p_x and p_y, "Need a direction" )
    self.direction  =   vector.new( p_x, p_y )
end

function Particle:setColor( p_color )
    self.color  =   p_color
end

function Particle:setLifeTime( p_lifetime )
    self.lifetime   =   p_lifetime
end
