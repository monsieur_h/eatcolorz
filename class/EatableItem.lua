------------------------------------------------------------------------
-- @class EatableItem
-- Class representing the player
------------------------------------------------------------------------


class "EatableItem"{
}

function EatableItem:__init__( p_x, p_y, p_value, p_colorstring )
    assert( p_x and p_y, "EatableItem needs coordinate")
    self.value      =   p_value or 50
    self.pos        =   vector(p_x, p_y)
    self.eaten      =   false
    self.offset     =   clamp( self.value, 10, 50 )
    self.alpha      =   255
    self.time       =   0
    self.dead       =   false
    self.collidables=   {}
    self.colorstring=   p_colorstring or "random"

	-- Determining randoom color
	if self.colorstring == "random" then
		local colorid	=	math.random(1, 3)
		if math.floor(colorid)	==	1 then self.colorstring = "red" end
		if math.floor(colorid)	==	2 then self.colorstring = "green" end
		if math.floor(colorid)	==	3 then self.colorstring = "blue" end
	end

	-- Applying color
    if self.colorstring == "red" then
        self.color      =   {r=255, g=0, b=0}
    elseif self.colorstring == "green" then
        self.color      =   {r=0, g=255, b=0}
    else
        self.color      =   {r=0, g=0, b=255}
    end

end

function EatableItem:update( dt )
    self.time   =   self.time + dt
    if self.eaten then
        -- move toward mouth
        local delta =   self.time - self.t_eaten
        if delta < self.eaten_duration then
            self.alpha  =   255 - ( 255 * delta/self.eaten_duration )
            self.pos    =   self.pos + ( self.v_direction * self.v_speed * dt)
        else
            self.dead   =   true
        end
    elseif not self:isOnGround() then
        self:fall( dt )
    end
end

function EatableItem:draw()
    love.graphics.setColor( self.color.r, self.color.g, self.color.b, self.alpha )
    love.graphics.circle( "fill", self.pos.x, self.pos.y, self.offset )
    love.graphics.setColor( 0, 0, 0, self.alpha )
    love.graphics.setLine( 7, "smooth" )
    love.graphics.circle( "line", self.pos.x, self.pos.y, self.offset )
end

function EatableItem:getsEaten( p_pos, p_duration )
    if not self.eaten then
        self.eaten = true
        local v_direction   =   p_pos - self.pos
        self.v_speed        =   v_direction:len() / p_duration
        self.v_direction    =   v_direction:normalized()
        self.t_eaten        =   self.time
        self.eaten_duration =   p_duration
        self.fadeout        =   p_duration * 255 * 1000
    end
end

function EatableItem:isOnGround()
    for i, col in pairs( self.collidables ) do
        if col:collides( self.pos.x, self.pos.y + self.offset)then
            if col.dangerous then
                self:die()
            else
                self.pos.y  =   col.rect.y - self.offset
            end
            return true
        end
    end

    --Tester le sol
    if self.pos.y >= love.graphics.getHeight() - self.offset then -- 40 parce que!
        return true
    else
        return false
    end
end

function EatableItem:fall( dt )
    self.pos.y  =   self.pos.y + 600 * dt
end

function EatableItem:addCollidable( p_colli )
    assert( p_colli )
    table.insert( self.collidables, p_colli )
end

function EatableItem:die()
    self.dead   =   true
end

function EatableItem:toConstructorString()
	return "table.insert( level.powerups, EatableItem( "..self.pos.x..", "..self.pos.y..", "..self.value..", \""..self.colorstring.."\" ) )"
end
