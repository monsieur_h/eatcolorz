-- Util class Rectangle to keep track of coordinates and make quick tests

class "Rectangle"{
}

function Rectangle:__init__(p_x, p_y, p_width, p_height)
    assert(p_x, "Rectangle needs X")
    assert(p_y, "Rectangle needs Y")
    assert(p_width, "Rectangle needs width")
    assert(p_height, "Rectangle needs height")
    self.x=p_x
    self.y=p_y
    self.width=p_width
    self.height=p_height

    self.center_x=self.x + self.width  / 2
    self.center_y=self.y + self.height / 2

end

function Rectangle:contains(p_x, p_y)
    local x_ok=false
    local y_ok=false
    if p_x > self.x and p_x < self.x + self.width then
        x_ok=true
    end

    if p_y > self.y and p_y < self.y + self.height then
        y_ok=true
    end

    return (x_ok and y_ok)
end

function Rectangle:testPoint(p_x, p_y)
    return self:contains(p_x, p_y)
end

function Rectangle:getCenter()
    return self.center_x, self.center_y

end

function Rectangle:setCenter(p_x, p_y)
    assert(p_x, "setCenter needs X coordinate")
    assert(p_y, "setCenter needs Y coordinate")
    self.x=p_x - self.width/2
    self.y=p_y - self.height/2

    self.center_x=p_x
    self.center_y=p_y

end

function Rectangle:draw()
    love.graphics.rectangle("line", self.x, self.y, self.width, self.height)

end

function Rectangle:getDrawable(p_mode)
    local mode = p_mode or "line"
    return mode, self.x, self.y, self.width, self.height
end

function Rectangle:inflate_inplace(p_width, p_height)
    self.x      = self.x - p_width      / 2
    self.width  = self.width + p_width  / 2
    self.y      = self.y - p_height     / 2
    self.height = self.height + p_height/ 2

end

function Rectangle:inflate(p_width, p_height)
    local r     = Rectangle(0, 0, self.width + p_width, self.height + p_height)
    r:setCenter(self.center_x, self.center_y)
    return r

end

function Rectangle:setPos(p_x, p_y)
    self.x  =   p_x
    self.y  =   p_y
    self.center_x   =   p_x + self.width/2
    self.center_y   =   p_y + self.height/2
end

function Rectangle:topleft()
	return self.x, self.y
end

function Rectangle:topright()
	return self.x+self.width, self.y
end

function Rectangle:bottomleft()
	return self.x, self.y+self.height
end

function Rectangle:bottomright()
	return self.x+self.width, self.y+self.height
end
