-- Util class Platform to keep track of coordinates and make quick tests

require "class.Platform"
class "DeathTrap" (Platform){
}


function DeathTrap:draw()
    local r = self.rect
    love.graphics.setColor( 255, 0, 0, 255 )
    love.graphics.rectangle( "fill", r.x, r.y, r.width, r.height )
    love.graphics.setColor( 0, 0, 0, 255 )
    love.graphics.setLine( 13, "smooth" )
    --~ love.graphics.rectangle( "line", r.x, r.y, r.width, r.height )

    love.graphics.line( r.x+r.width, r.y, r.x+r.width, r.y+r.height, r.x, r.y+r.height, r.x, r.y  )
    local offset= 10
    local peaks = r.width / offset
    local list  = {}
    local up    = 1
    table.insert(list, r.x)
    table.insert(list, r.y+offset/2)
    for i = 1, peaks do
        if i%2~=0 then --X coordinate
            table.insert(list, r.x+i*offset)
        else
            up = up * -1
            table.insert(list, r.y+offset*up)
        end
    end
	if math.mod(#list, 2) ~= 0 then
	--table.insert(list, r.x+r.width)
		table.insert(list, r.y+offset/2)
		--table.insert( list, self.rect.x + self.rect.width )
	end
	table.insert( list, r.x+r.width )
	table.insert( list, r.y )
    love.graphics.line( unpack(list) )
end

function DeathTrap:collides( p_x, p_y )
    if not self.dangerous then self.dangerous=true end
    if p_x >= self.rect.x
        and p_x <= self.rect.x + self.rect.width
        and p_y >= self.rect.y
        and p_y <= self.rect.y + 50
    then
        return true
    else
        return false
    end
end

--Dessinner le rectangle TODO: couleur a revoir
    --~ love.graphics.setColor( self.player.color.r/2, self.player.color.g/2, self.player.color.b/2, 255 )
    --~ love.graphics.setLine( 10, "smooth" )
    --~ love.graphics.rectangle( "fill", r.x, r.y, r.width, r.height )
    --~ --Dessinner la ligne de contour
    --~ love.graphics.setColor( 0, 0, 0, 255 )
    --~ r:draw()
function DeathTrap:toConstructorString()
	local cx, cy	=	self.rect:getCenter()
	return "table.insert( level.platforms, DeathTrap( "..self.rect.width..", "..self.rect.height..", "..cx..", "..cy.." ) )"
end
